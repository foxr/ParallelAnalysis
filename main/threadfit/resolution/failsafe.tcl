#  SpecTclGUI save file created Mon Jul 02 16:10:02 EDT 2018
#  SpecTclGui Version: 1.0
#      Author: Ron Fox (fox@nscl.msu.edu)

#Tree params:

catch {treeparameter -create event.actual.left.amplitude 1 100 100 unknown}
treeparameter -setlimits event.actual.left.amplitude 1 100
treeparameter -setbins   event.actual.left.amplitude 100
treeparameter -setunit   event.actual.left.amplitude unknown

catch {treeparameter -create event.actual.left.decayTime 1 100 100 unknown}
treeparameter -setlimits event.actual.left.decayTime 1 100
treeparameter -setbins   event.actual.left.decayTime 100
treeparameter -setunit   event.actual.left.decayTime unknown

catch {treeparameter -create event.actual.left.position 1 100 100 unknown}
treeparameter -setlimits event.actual.left.position 1 100
treeparameter -setbins   event.actual.left.position 100
treeparameter -setunit   event.actual.left.position unknown

catch {treeparameter -create event.actual.left.steepness 1 100 100 unknown}
treeparameter -setlimits event.actual.left.steepness 1 100
treeparameter -setbins   event.actual.left.steepness 100
treeparameter -setunit   event.actual.left.steepness unknown

catch {treeparameter -create event.actual.right.amplitude 1 100 100 unknown}
treeparameter -setlimits event.actual.right.amplitude 1 100
treeparameter -setbins   event.actual.right.amplitude 100
treeparameter -setunit   event.actual.right.amplitude unknown

catch {treeparameter -create event.actual.right.decayTime 1 100 100 unknown}
treeparameter -setlimits event.actual.right.decayTime 1 100
treeparameter -setbins   event.actual.right.decayTime 100
treeparameter -setunit   event.actual.right.decayTime unknown

catch {treeparameter -create event.actual.right.position 1 100 100 unknown}
treeparameter -setlimits event.actual.right.position 1 100
treeparameter -setbins   event.actual.right.position 100
treeparameter -setunit   event.actual.right.position unknown

catch {treeparameter -create event.actual.right.steepness 1 100 100 unknown}
treeparameter -setlimits event.actual.right.steepness 1 100
treeparameter -setbins   event.actual.right.steepness 100
treeparameter -setunit   event.actual.right.steepness unknown

catch {treeparameter -create event.actualOffset 1 100 100 unknown}
treeparameter -setlimits event.actualOffset 1 100
treeparameter -setbins   event.actualOffset 100
treeparameter -setunit   event.actualOffset unknown

catch {treeparameter -create event.chiratio2over1 1 100 100 unknown}
treeparameter -setlimits event.chiratio2over1 1 100
treeparameter -setbins   event.chiratio2over1 100
treeparameter -setunit   event.chiratio2over1 unknown

catch {treeparameter -create event.dt 1 100 100 unknown}
treeparameter -setlimits event.dt 1 100
treeparameter -setbins   event.dt 100
treeparameter -setunit   event.dt unknown

catch {treeparameter -create event.dtdiff 1 100 100 unknown}
treeparameter -setlimits event.dtdiff 1 100
treeparameter -setbins   event.dtdiff 100
treeparameter -setunit   event.dtdiff unknown

catch {treeparameter -create event.fit1.AmplitudeDiff 1 100 100 unknown}
treeparameter -setlimits event.fit1.AmplitudeDiff 1 100
treeparameter -setbins   event.fit1.AmplitudeDiff 100
treeparameter -setunit   event.fit1.AmplitudeDiff unknown

catch {treeparameter -create event.fit1.Tdiff 1 100 100 unknown}
treeparameter -setlimits event.fit1.Tdiff 1 100
treeparameter -setbins   event.fit1.Tdiff 100
treeparameter -setunit   event.fit1.Tdiff unknown

catch {treeparameter -create event.fit2.T0Diff 1 100 100 unknown}
treeparameter -setlimits event.fit2.T0Diff 1 100
treeparameter -setbins   event.fit2.T0Diff 100
treeparameter -setunit   event.fit2.T0Diff unknown

catch {treeparameter -create event.fit2.T1Diff 1 100 100 unknown}
treeparameter -setlimits event.fit2.T1Diff 1 100
treeparameter -setbins   event.fit2.T1Diff 100
treeparameter -setunit   event.fit2.T1Diff unknown

catch {treeparameter -create event.fit2.a1diff 1 100 100 unknown}
treeparameter -setlimits event.fit2.a1diff 1 100
treeparameter -setbins   event.fit2.a1diff 100
treeparameter -setunit   event.fit2.a1diff unknown

catch {treeparameter -create event.fit2.a2diff 1 100 100 unknown}
treeparameter -setlimits event.fit2.a2diff 1 100
treeparameter -setbins   event.fit2.a2diff 100
treeparameter -setunit   event.fit2.a2diff unknown

catch {treeparameter -create event.fits.onepulsefit.chiSquare 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.chiSquare 1 100
treeparameter -setbins   event.fits.onepulsefit.chiSquare 100
treeparameter -setunit   event.fits.onepulsefit.chiSquare unknown

catch {treeparameter -create event.fits.onepulsefit.fit.amplitude 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.fit.amplitude 1 100
treeparameter -setbins   event.fits.onepulsefit.fit.amplitude 100
treeparameter -setunit   event.fits.onepulsefit.fit.amplitude unknown

catch {treeparameter -create event.fits.onepulsefit.fit.decayTime 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.fit.decayTime 1 100
treeparameter -setbins   event.fits.onepulsefit.fit.decayTime 100
treeparameter -setunit   event.fits.onepulsefit.fit.decayTime unknown

catch {treeparameter -create event.fits.onepulsefit.fit.position 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.fit.position 1 100
treeparameter -setbins   event.fits.onepulsefit.fit.position 100
treeparameter -setunit   event.fits.onepulsefit.fit.position unknown

catch {treeparameter -create event.fits.onepulsefit.fit.steepness 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.fit.steepness 1 100
treeparameter -setbins   event.fits.onepulsefit.fit.steepness 100
treeparameter -setunit   event.fits.onepulsefit.fit.steepness unknown

catch {treeparameter -create event.fits.onepulsefit.fitStatus 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.fitStatus 1 100
treeparameter -setbins   event.fits.onepulsefit.fitStatus 100
treeparameter -setunit   event.fits.onepulsefit.fitStatus unknown

catch {treeparameter -create event.fits.onepulsefit.iterations 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.iterations 1 100
treeparameter -setbins   event.fits.onepulsefit.iterations 100
treeparameter -setunit   event.fits.onepulsefit.iterations unknown

catch {treeparameter -create event.fits.onepulsefit.offset 1 100 100 unknown}
treeparameter -setlimits event.fits.onepulsefit.offset 1 100
treeparameter -setbins   event.fits.onepulsefit.offset 100
treeparameter -setunit   event.fits.onepulsefit.offset unknown

catch {treeparameter -create event.fits.twopulsefit.chiSquare 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.chiSquare 1 100
treeparameter -setbins   event.fits.twopulsefit.chiSquare 100
treeparameter -setunit   event.fits.twopulsefit.chiSquare unknown

catch {treeparameter -create event.fits.twopulsefit.fitStatus 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.fitStatus 1 100
treeparameter -setbins   event.fits.twopulsefit.fitStatus 100
treeparameter -setunit   event.fits.twopulsefit.fitStatus unknown

catch {treeparameter -create event.fits.twopulsefit.iterations 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.iterations 1 100
treeparameter -setbins   event.fits.twopulsefit.iterations 100
treeparameter -setunit   event.fits.twopulsefit.iterations unknown

catch {treeparameter -create event.fits.twopulsefit.offset 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.offset 1 100
treeparameter -setbins   event.fits.twopulsefit.offset 100
treeparameter -setunit   event.fits.twopulsefit.offset unknown

catch {treeparameter -create event.fits.twopulsefit.pulse1.amplitude 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse1.amplitude 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse1.amplitude 100
treeparameter -setunit   event.fits.twopulsefit.pulse1.amplitude unknown

catch {treeparameter -create event.fits.twopulsefit.pulse1.decayTime 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse1.decayTime 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse1.decayTime 100
treeparameter -setunit   event.fits.twopulsefit.pulse1.decayTime unknown

catch {treeparameter -create event.fits.twopulsefit.pulse1.position 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse1.position 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse1.position 100
treeparameter -setunit   event.fits.twopulsefit.pulse1.position unknown

catch {treeparameter -create event.fits.twopulsefit.pulse1.steepness 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse1.steepness 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse1.steepness 100
treeparameter -setunit   event.fits.twopulsefit.pulse1.steepness unknown

catch {treeparameter -create event.fits.twopulsefit.pulse2.amplitude 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse2.amplitude 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse2.amplitude 100
treeparameter -setunit   event.fits.twopulsefit.pulse2.amplitude unknown

catch {treeparameter -create event.fits.twopulsefit.pulse2.decayTime 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse2.decayTime 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse2.decayTime 100
treeparameter -setunit   event.fits.twopulsefit.pulse2.decayTime unknown

catch {treeparameter -create event.fits.twopulsefit.pulse2.position 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse2.position 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse2.position 100
treeparameter -setunit   event.fits.twopulsefit.pulse2.position unknown

catch {treeparameter -create event.fits.twopulsefit.pulse2.steepness 1 100 100 unknown}
treeparameter -setlimits event.fits.twopulsefit.pulse2.steepness 1 100
treeparameter -setbins   event.fits.twopulsefit.pulse2.steepness 100
treeparameter -setunit   event.fits.twopulsefit.pulse2.steepness unknown

catch {treeparameter -create event.fittedDt 1 100 100 unknown}
treeparameter -setlimits event.fittedDt 1 100
treeparameter -setbins   event.fittedDt 100
treeparameter -setunit   event.fittedDt unknown

catch {treeparameter -create event.isdouble 1 100 100 unknown}
treeparameter -setlimits event.isdouble 1 100
treeparameter -setbins   event.isdouble 100
treeparameter -setunit   event.isdouble unknown


# Pseudo parameter definitions


# Tree variable definitions:


# Gate definitions in reverse dependency order
 

# Spectrum Definitions

catch {spectrum -delete Actua.a1}
spectrum Actua.a1 1 event.actual.left.amplitude {{0.000000 1024.000000 1024}} long
catch {spectrum -delete Actua.a2}
spectrum Actua.a2 1 event.actual.right.amplitude {{0.000000 1024.000000 1024}} long
catch {spectrum -delete Chisquare1}
spectrum Chisquare1 1 event.fits.onepulsefit.chiSquare {{0.000000 5000.000000 20000}} long
catch {spectrum -delete Chisquare2}
spectrum Chisquare2 1 event.fits.twopulsefit.chiSquare {{0.000000 4000.000000 20000}} long
catch {spectrum -delete EventType}
spectrum EventType 1 event.isdouble {{0.000000 400.000000 100}} long
catch {spectrum -delete a1diff}
spectrum a1diff 1 event.chiratio2over1 {{1.000000 100.000000 100}} long
catch {spectrum -delete a2diff}
spectrum a2diff 1 event.fit2.a1diff {{-1000.000000 1000.000000 2000}} long
catch {spectrum -delete actualdt-v-fita2diff}
spectrum actualdt-v-fita2diff 2 {event.dt event.fit2.a2diff} {{0.000000 100.000000 100} {-500.000000 500.000000 1000}} long
catch {spectrum -delete actualdt-v-fitdtdiff}
spectrum actualdt-v-fitdtdiff 2 {event.dt event.dtdiff} {{0.000000 100.000000 100} {-200.000000 100.000000 300}} long
catch {spectrum -delete chratio}
spectrum chratio 1 event.chiratio2over1 {{0.000000 10.000000 200}} long
catch {spectrum -delete dtdiff}
spectrum dtdiff 1 event.dtdiff {{-5000.000000 5010.000000 5100}} long
catch {spectrum -delete f1.position}
spectrum f1.position 1 event.fits.onepulsefit.fit.position {{0.000000 500.000000 500}} long
catch {spectrum -delete f2.p1}
spectrum f2.p1 1 event.fits.twopulsefit.pulse1.position {{0.000000 500.000000 500}} long
catch {spectrum -delete f2.p2}
spectrum f2.p2 1 event.fits.twopulsefit.pulse2.position {{0.000000 500.000000 500}} long
catch {spectrum -delete fit1.a}
spectrum fit1.a 1 event.fits.onepulsefit.fit.amplitude {{0.000000 1024.000000 1024}} long
catch {spectrum -delete fit1.steepness}
spectrum fit1.steepness 1 event.fits.onepulsefit.fit.steepness {{-2.000000 2.000000 100}} long
catch {spectrum -delete fit2.a1}
spectrum fit2.a1 1 event.fits.twopulsefit.pulse1.amplitude {{0.000000 1024.000000 1024}} long
catch {spectrum -delete fit2.a2}
spectrum fit2.a2 1 event.fits.twopulsefit.pulse2.amplitude {{0.000000 1024.000000 1024}} long
catch {spectrum -delete fit2.left.steepness}
spectrum fit2.left.steepness 1 event.fits.twopulsefit.pulse1.steepness {{-2.000000 2.000000 100}} long
catch {spectrum -delete fit2.right.steepness}
spectrum fit2.right.steepness 1 event.fits.twopulsefit.pulse2.steepness {{-2.000000 2.000000 100}} long
catch {spectrum -delete t1Actual}
spectrum t1Actual 1 event.actual.left.position {{0.000000 500.000000 501}} long
catch {spectrum -delete t2Actual}
spectrum t2Actual 1 event.actual.right.position {{0.000000 500.000000 501}} long

# Gate Applications: 


#  filter definitions: ALL FILTERS ARE DISABLED!!!!!!!


#  - Parameter tab layout: 

set parameter(select) 1
set parameter(Array)  false

#-- Variable tab layout

set variable(select) 1
set variable(Array)  0
